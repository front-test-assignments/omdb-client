import image2 from './images/image2.jpg'
import image3 from './images/image3.jpg'
import image4 from './images/image4.jpg'
import image5 from './images/image5.jpg'

export const filmsList = [
  {
    name: 'Peaky Blinders',
    ganre: 'Crime, Drama',
    type: 'TV Series 2013',
    about: 'A gangster family epic set in 1900s England, centering on a gang who sew razor blades in the peaks of their caps, and their fierce boss Tommy Shelby.',
    raiting: 'IMDB 8.8',
    image:`${image3}`
  },
  {
    name: 'Game of Thrones',
    ganre: 'Adventure, Drama',
    type: ' TV Series (2011–2019)',
    about: 'Nine noble families fight for control over the lands of Westeros, while an ancient enemy returns after being dormant for millennia.',
    raiting: 'IMDB 9.3',
    image:`${image2}`
  },
  {
    name: 'Breaking Bad',
    ganre: 'DramaDrama, Thriller',
    type: ' TV Series (2008–2013)',
    about: 'A high school chemistry teacher diagnosed with inoperable lung cancer turns to manufacturing and selling methamphetamine in order to secure his family`s future.',
    raiting: 'IMDB 9.5',
    image:`${image4}`
  },
  {
    name: 'Stranger Things',
    ganre: 'Fantasy, Horror',
    type: ' TV Series (2016– )',
    about: 'When a young boy disappears, his mother, a police chief and his friends must confront terrifying supernatural forces in order to get him back.',
    raiting: 'IMDB 8.7',
    image:`${image5}`
  }
]