import styled from 'styled-components';

const FilmCard = styled.div`
  display:flex;
  width: 263px;
  height: 360px;
  margin: 2px;
  transition: all 0.5s ease;
  justify-content:center;
  align-items:center;
`

const DisplayedPoster = styled.div`
  border-radius: 16px;
  display: flex;
  width: 100%;
  height: 100%;
  transition: all 0.5s ease;
  background-image: ${props => `url(${props.bacgroundImage})`};
  filter: brightness(100%)
`
const CardInfo = styled.div`
  position: absolute;
  visibility: hidden;
  width: 220px;
  height: 320px;
`
const CardInfoWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-self: center;
  width:100%;
  height: 100%;
  justify-content: center;
  align-items: self-start;
`

const Raiting = styled.div`
  width:87px;
  height:30px;
  background-color:#FAC539;
  border-radius:8px;
  align-items:center;
  display:flex;
  justify-content:center;
  margin-top:16px;
  font-family: 'Montserrat', sans-serif;
  font-weight: 900;
  font-size: 12px;
  color: black;
  text-align: center;
`
const Header = styled.div`
font-family: 'Montserrat', sans-serif;
color: #323232;
font-size: 24px;
font-weight: 900;
margin-top: 60px;
margin-bottom: 20px;
`
const CardHeader = styled.div`
  font-family: 'Montserrat', sans-serif;
  color: #FFFFFF;
  font-size: 24px;
  font-weight: 800;
  width:'50%';
`
const CardMiddle = styled.div`
  font-family: 'Montserrat', sans-serif;
  color: #FFFFFF;
  font-size: 14px;
  font-weight: 500;
  margin-top: 8px;
`
const CardFotter = styled.div`
  font-family: 'Montserrat', sans-serif;
  color: #FFFFFF;
  font-size: 14px;
  font-weight: 400;
  margin-top: 21px;
`
export {FilmCard, DisplayedPoster, CardInfo, Raiting, Header, CardHeader, CardMiddle, CardFotter, CardInfoWrapper} 