import { filmsList } from './filmsList'
import { useState } from 'react';
import {
  FilmCard, DisplayedPoster, CardInfo,
  Raiting, Header, CardHeader, 
  CardMiddle, CardFotter, CardInfoWrapper
} from './index'

const FilmsLikeThis = () => {
  const [event, setEvent] = useState()
  const chengeCard = (e) => {
    if (e) {
      e.target.style.filter = 'brightness(30%)'
      e._targetInst.stateNode.nextSibling.style.visibility = 'visible'
      setEvent(e)
    }
  }
  
  const disChengeCard = () => {
    if (event) {
      event.target.style.filter = `brightness(100%)`
      event._targetInst.stateNode.nextSibling.style.visibility = 'hidden'

    }
  }

  const filmsCardsRender = () => {
    return (
      filmsList.map((film) => {
        return (
          <FilmCard onMouseEnter={chengeCard} onMouseLeave={disChengeCard}>
            <DisplayedPoster bacgroundImage={film.image}></DisplayedPoster>
            <CardInfo>
              <CardInfoWrapper>
                <CardHeader>{film.name}</CardHeader>
                <CardMiddle>{film.ganre}</CardMiddle>
                <CardMiddle>{film.type}</CardMiddle>
                <CardFotter>{film.about}</CardFotter>
                <Raiting>{film.raiting}</Raiting>
              </CardInfoWrapper>
            </CardInfo>
          </FilmCard>
        )
      })
    )
  }
  return (
    <div>
      <Header onMouseEnter={disChengeCard}>
        You may also like
      </Header>
      <div style={{display:'flex'}} onMouseEnter={disChengeCard} id='cardArea'>
        {filmsCardsRender()}
      </div>
    </div>
  )
}

export default FilmsLikeThis