import { Link } from 'react-router-dom';

const FilmPreview = ({film}) => {
    console.log(film)
    // const film = film
    // const film = {
        
    //         "Title": "The Queen's Gambit",
    //         "Year": "2020",
    //         "Rated": "TV-MA",
    //         "Released": "23 Oct 2020",
    //         "Runtime": "395 min",
    //         "Genre": "Drama, Sport",
    //         "Director": "N/A",
    //         "Writer": "N/A",
    //         "Actors": "Anya Taylor-Joy, Chloe Pirrie, Bill Camp, Marcin Dorocinski",
    //         "Plot": "Orphaned at the tender age of nine, prodigious introvert Beth Harmon discovers and masters the game of chess in 1960s USA. But child stardom comes at a price.",
    //         "Language": "French, Spanish, Russian, English",
    //         "Country": "USA",
    //         "Awards": "Won 2 Golden Globes. Another 13 wins & 21 nominations.",
    //         "Poster": "https://m.media-amazon.com/images/M/MV5BM2EwMmRhMmUtMzBmMS00ZDQ3LTg4OGEtNjlkODk3ZTMxMmJlXkEyXkFqcGdeQXVyMjM5ODk1NDU@._V1_SX300.jpg",
    //         "Ratings": [
    //             {
    //                 "Source": "Internet Movie Database",
    //                 "Value": "8.6/10"
    //             }
    //         ],
    //         "Metascore": "N/A",
    //         "imdbRating": "8.6",
    //         "imdbVotes": "282,585",
    //         "imdbID": "tt10048342",
    //         "Type": "series",
    //         "totalSeasons": "1",
    //         "Response": "True"
        
    // }

    const filmClickHandler = () => {

    }
    return (
        <div className='filmPreview' style={{width:'555px', height:'171px', backgroundColor:'#111111bd', borderRadius:'10px', flexDirection:'row', display:'flex', justifyContent:'center'}} onClick={filmClickHandler}>
            <div style={{width:'96%', height:'90%',flexDirection:'row', display:'flex', alignItems:'center', alignSelf:'center'}}>
                <div style={{alignItems:'center', display:'flex', maxWidth:'20%', height:'100%'}}>
                    <img src={film.Poster} alt={film.Title} style={{height:'auto', width: '95%', borderRadius: '5px', alignSelf: 'center', display:'flex'}}/>
                </div>
                <div style={{width:'90%', marginTop:'15px',/* alignSelf:'center'*/}}>
                    <div>
                        <div style={{display:'flex', justifyContent:'space-between'}}>
                            <Link className='preview-font-family' style={{color: '#FEFEFE'}} to={`/film/${film.imdbID}`}>{film.Title}</Link>
                            <div style={{width:'87px', height:'30px', backgroundColor:'#FAC539', borderRadius:'8px', alignItems:'center', display:'flex', justifyContent:'center', }} className='preview-raiting-font-family'>
                                IMDB {film.imdbRating}
                            </div>
                        </div>
                    {/* <p style={{}} className='preview-mini-font-family'>{film.Rated}{film.Genre}{film.Year}</p> */}
                    <div className='preview-mini-font-family' style={{display:'flex', alignItems:'stretch', marginBottom:'15px'}}>
                        <div style={{paddingLeft: '0px', paddingRight:'10px'}}>{film.Rated}</div>
                        <div style={{borderLeft: '1px solid white',borderRight: '1px solid white', paddingLeft: '10px', paddingRight:'10px'}}>{film.Genre}</div>
                        <div style={{paddingLeft: '10px', paddingRight:'0px'}}>{film.Year}</div>
                    </div>
                    </div>
                    <div className='preview-buttom-font-family' style={{borderTop:'1px solid rgb(255 255 255 / 21%)', alignItems:'center', height:'50px', display:'flex'}}>
                        {film.Awards}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default FilmPreview