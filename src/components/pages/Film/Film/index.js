import styled from 'styled-components';
import img from './image1.jpg'

const FilmMainBlock = styled.div`
  background-image: url(${img});
  width:100%;
  height:810px;
  background-repeat: no-repeat;
  background-size: cover;
  flex-direction: 'column';
`


const Raiting = styled.div`
  width:87px;
  height:30px;
  background-color:#FAC539;
  border-radius:8px;
  align-items:center;
  display:flex;
  justify-content:center;
  font-family: 'Montserrat', sans-serif;
  font-weight: 900;
  font-size: 12px;
  color: black;
`
const Wrapper = styled.div`
  padding-left: 150px;
  padding-right: 150px;
  height: 100%;
  width: 100%;
  display: flex;
//   background-color: black;
  flex-direction: column;
`
const Header = styled.div`
  // margin-top: 60px;
  height: 100px;
  width: 100%;
  background-color: black;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`
const FilmMainBlockInfo = styled.div`
  padding-left: 150px;
  padding-right: 150px;
  height: 100%;
  width: 80%;
`


const FilmMainInfoName = styled.div`
font-family: 'Montserrat', sans-serif;
font-weight: 900;
font-size: 72px;
color: #FEFEFE;
padding-top: 120px;
`

const FilmMainInfoOther = styled.div`
font-family: 'Montserrat', sans-serif;
font-weight: 300;
font-size: 24px;
color: #FEFEFE;
display: flex; 
flex-direction: row;
align-items: center;
`
const FilmMainInfoOtherItem = styled.div`
font-family: 'Montserrat', sans-serif;
font-weight: 300;
font-size: 24px;
color: #FEFEFE;
display: flex; 
flex-direction: row;
align-items: center;
padding-left: 10px;
align-self: center;
`
const HederLink = styled.a`
font-family: 'Montserrat', sans-serif;
font-weight: 300;
font-size: 24px;
color: #FEFEFE !important;
align-self: center;
`
const FilmSearcher = styled.input`
padding-left: 20px;
color: #5F5F5F;
border-radius: 30px;
background-color: #1B1919;
border-color: #1B1919;
width: 420px;
height: 50px; 
align-self: center;
font-family: 'Montserrat', sans-serif;
font-weight: 400;
font-size: 24px;
`
const FilmBottomBlockHeader = styled.div`
margin-top: 60px;
font-family: 'Montserrat', sans-serif;
color: #323232;
font-size: 36px;
font-weight: 900;
width: 850px;

`
const FilmBottomBlockInfo = styled.div`
font-family: 'Montserrat', sans-serif;
color: #323232;
font-size: 18px;
font-weight: 400; 
margin-top: 80px;
display: flex;
flex-direction: column;
height: 100%;
width: 100%;
`
const Footer = styled.a`
  width: 100%;
  height: 100px;
  background-color: #111111;
  margin-top: 60px;
  font-family: 'Montserrat', sans-serif;
  font-weight: 900;
  font-size: 18px;
  color: #FEFEFE !important;
  display: flex;
  align-items: center;
  justify-content: center;
`

export { 
  FilmMainBlock, FilmMainBlockInfo, Header, Raiting, Wrapper, 
  FilmMainInfoName, FilmMainInfoOther, FilmMainInfoOtherItem, HederLink, 
  FilmSearcher, FilmBottomBlockInfo, FilmBottomBlockHeader, Footer
}