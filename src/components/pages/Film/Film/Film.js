import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import {FilmSearch} from '../../FilmSearch/FilmSearch';
import { fetchFilmById, fetchFilmsByQuery } from '../../../store/thunks'
import { Input } from 'antd';
import { 
  FilmMainBlock, FilmMainBlockInfo, Header, Raiting, 
  Wrapper, FilmMainInfoName, FilmMainInfoOther, FilmMainInfoOtherItem, HederLink, 
  FilmSearcher, FilmBottomBlockInfo, FilmBottomBlockHeader, Footer
} from '.'


import FilmsLikeThis from '../FilmsLikeThis/FilmsLikeThis';
import styled from 'styled-components';

const { Search } = Input;
// const film = {
//     "Title":"Star Wars: Episode IV - A New Hope",
//     "Year":"1977",
//     "Rated":"PG",
//     "Released":"25 May 1977",
//     "Runtime":"121 min",
//     "Genre":"Action, Adventure, Fantasy, Sci-Fi",
//     "Director":"George Lucas",
//     "Writer":"George Lucas",
//     "Actors":"Mark Hamill, Harrison Ford, Carrie Fisher, Peter Cushing",
//     "Plot":"The Imperial Forces, under orders from cruel Darth Vader, hold Princess Leia hostage in their efforts to quell the rebellion against the Galactic Empire. Luke Skywalker and Han Solo, captain of the Millennium Falcon, work together with the companionable droid duo R2-D2 and C-3PO to rescue the beautiful princess, help the Rebel Alliance and restore freedom and justice to the Galaxy.",
//     "Language":"English",
//     "Country":"USA",
//     "Awards":"Won 6 Oscars. Another 52 wins & 29 nominations.",
//     "Poster":"https://m.media-amazon.com/images/M/MV5BNzVlY2MwMjktM2E4OS00Y2Y3LWE3ZjctYzhkZGM3YzA1ZWM2XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SX300.jpg",
//     "Ratings":[{"Source":"Internet Movie Database","Value":"8.6/10"},{"Source":"Rotten Tomatoes","Value":"92%"},{"Source":"Metacritic","Value":"90/100"}],"Metascore":"90","imdbRating":"8.6","imdbVotes":"1,208,256","imdbID":"tt0076759",
//     "Type":"movie",
//     "DVD":"N/A",
//     "BoxOffice":"N/A",
//     "Production":"Lucasfilm Ltd.",
//     "Website":"N/A",
//     "Response":"True"
// }



const info = "Nine year-old orphan Beth Harmon is quiet, sullen, and by all appearances unremarkable. That is, until she plays her first game of chess. Her senses grow sharper, her thinking clearer, and for the first time in her life she feels herself fully in control. By the age of sixteen, she's competing for the U.S. Open championship. But as Beth hones her skills on the professional circuit, the stakes get higher, her isolation grows more frightening, and the thought of escape becomes all the more tempting. Based on the book by Walter Tevis."

const Film = ({dispatch, film}) => {
  const { filmId } = useParams();

  const [ isLoading, setIsLoading ] = useState(false);

  useEffect(() => {
    dispatch(fetchFilmById(filmId))
  }, [filmId])

  const renderLoadingPage = () => {
    return (
      <>
        <div style={{width: '490px', margin: 'auto'}}>
          <iframe src="https://gifer.com/embed/3vTB" width={480} height="545.280" frameBorder={0} allowFullScreen title='humster'/>
        </div>
      </>
    )
  }

  const openInNewTab = (url) => {
    const newWindow = window.open(url, '_blank', 'noopener,noreferrer')
    if (newWindow) newWindow.opener = null
  }
  console.log(film)
  const handleSearchFieldSubmit = (searchValue) => {
    console.log(searchValue)
    dispatch(fetchFilmById(searchValue))
  }
  const renderInfoPage = (film) => {
    return (
      <>
        <Wrapper style={{backgroundColor:'black'}}>
          <Header>
            <HederLink href='/'>Richbee Shows</HederLink>
            <Input
            placeholder="Type here smth..."
            allowClear
            size="large"
            onPressEnter={e=>handleSearchFieldSubmit(e.target.value)}
            className='input my-input'
            bordered={false}
            style={{paddingLeft: '20px', color: '#5F5F5F', backgroundColor: '#1B1919', borderColor: '#1B1919 !important', width: '420px', height: '50px', 
            alignSelf: 'center', fontFamily: 'Montserrat', fontWeight: '400', fontSize: '24px'}}
            />
          </Header>
        </Wrapper>
        <FilmMainBlock>
          <FilmMainBlockInfo>
            <FilmMainInfoName>{film.Title}</FilmMainInfoName>
            <FilmMainInfoOther>
              <Raiting>
                IMDB {film.imdbRating}
              </Raiting>
              <FilmMainInfoOtherItem style={{paddingRight:'10px'}}>{film.Genre}</FilmMainInfoOtherItem>
              <FilmMainInfoOtherItem style={{paddingRight:'10px', borderLeft: '1px solid white', borderRight: '1px solid white'}}>{film.Rated}</FilmMainInfoOtherItem>
              <FilmMainInfoOtherItem style={{paddingRight:'0px'}}>{film.Year}</FilmMainInfoOtherItem>
            </FilmMainInfoOther>
            <div className='watch-batton' onClick={() => openInNewTab(`https://www.imdb.com/title/${film.imdbID}/`)}>
              Watch
            </div>
            <div className='film-main-awords-block'>
              {film.Awards}
            </div>
          </FilmMainBlockInfo>
        </FilmMainBlock>
        <Wrapper>
          <FilmBottomBlockHeader>
                Watch {film.Title} on Richbee Shows
          </FilmBottomBlockHeader>
          <FilmBottomBlockInfo>
              {info}
          </FilmBottomBlockInfo>
        </Wrapper>
          <Wrapper >
            <FilmsLikeThis/>
          </Wrapper>       
            <Footer href='/'>Richbee Shows</Footer>
      </>
    )
  }

  const renderPage = () => {
    if (isLoading) {
        return renderLoadingPage();
    }

    return renderInfoPage(film);
  }

  return (
    <>
      {renderPage()}
    </>
  )
}

const mapStateToProps = ({film}) => ({film});
export default connect(mapStateToProps)(Film);