import React from 'react';
import { Input } from 'antd';
import { connect } from 'react-redux'
import { fetchFilmById } from '../../store/thunks'
import { Button } from 'antd';
import FilmPreview from '../Film/FilmPreview'

const { Search } = Input;

const MainFilmSearchHeader = ({ dispatch, setSearchValue, film }) => {
  const handleSearchFieldSubmit = (searchValue) => {
    dispatch(fetchFilmById(searchValue))
    setSearchValue(searchValue)
  }
  console.log(film.Title)
  const filmslist = () => {
    if (film.Title) {
      return <FilmPreview film={film}/>
    }
  }
  return (

    <div style={{height:'100%', width:'100%', display: 'flex', flexDirection: 'column', backgroundColor:'transparent', position: 'absolute' ,zIndex: '1'}}>
        <div style={{alignSelf: 'center'}}>
            <p className='main-header-font-family' style={{color: '#FEFEFE', fontSize:'64px'}}>
                Unlimited movies,<br/> 
                TV shows, and more.
            </p>
            <p className='header-font-family' style={{color: '#FEFEFE', fontSize:'36px'}}>
                Watch anywhere. Cancel anytime.
            </p>
        </div>
        <Search
            placeholder="Type here smth..."
            allowClear
            enterButton="Search"
            size="large"
            onSearch={handleSearchFieldSubmit}
            className='input'
            style={{/*borderTopLeftRadius: '20px', borderBottomLeftRadius:'20px',*/ width:"555px", height:'50px', marginTop:'20px',/*, marginTop:'7px',*/ alignSelf: 'center',/* borderRadius:'20px', */backgroundColor:'transparent'}}
            />
        <div style={{marginTop: '20px', alignSelf: 'center', display:'flex', flexDirection:'column'}}>
            {filmslist()}
        </div>
    </div>
  )
  // export {handleSearchFieldSubmit}
}
const mapStateToProps = (state) => ({ film: state.film });
export default connect(mapStateToProps)(MainFilmSearchHeader)
 